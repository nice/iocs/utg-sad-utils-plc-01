#!/usr/bin/env iocsh.bash
require(plc_labs_utgard_ctrl_plc_01)

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases

# PLC databases
iocshLoad("$(plc_labs_utgard_ctrl_plc_01_DIR)/plc_labs_utgard_ctrl_plc_01.iocsh", "IPADDR=172.30.244.88, RECVTIMEOUT=1000")
